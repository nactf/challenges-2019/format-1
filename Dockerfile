FROM i386/buildpack-deps:buster as builder

COPY . /build/
WORKDIR /build/
RUN make

FROM i386/debian:buster

RUN apt-get update && apt-get install -y \
		socat \
	&& rm -rf /var/lib/apt/lists/*

RUN useradd user; mkdir /home/user
WORKDIR /home/user

COPY flag.txt ./
COPY --from=builder /build/format-1 ./

EXPOSE 12345
CMD while true; do socat TCP-LISTEN:12345,reuseaddr,fork,su=user EXEC:./format-1,stderr; done
